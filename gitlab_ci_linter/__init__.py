#!/usr/bin/env python3
import argparse
import json
import os
import ssl
import sys
import urllib.parse
import urllib.request


def parse_args():
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    parser.add_argument(
        "--server",
        default="https://gitlab.com",
        help="This server will check .gitlab-ci.yml",
    )
    parser.add_argument(
        "--filename", default=".gitlab-ci.yml", help="Specify Gitlab CI filename"
    )
    parser.add_argument(
        "--output-yaml",
        action="store_true",
        help="Output yaml on failure",
    )
    parser.add_argument(
        "-k",
        "--insecure",
        action="store_true",
        help="Allow insecure server connections when using SSL",
    )
    parser.add_argument(
        "--private-token",
        help="Use this private token to authenticate on the server",
        default=os.environ.get("GITLAB_PRIVATE_TOKEN"),
    )
    parser.add_argument(
        "--project",
        help="Gitlab project private-token is authorized for",
        default=os.environ.get("CI_PROJECT_ID"),
    )
    parser.add_argument(
        "--ref",
        default=os.environ.get("CI_COMMIT_REF_NAME", "main"),
        help="Reference to specify branch, when dry_run is true",
    )

    return parser.parse_args()


def main():
    args = parse_args()
    return gitlab_ci_linter(
        args.server,
        args.filename,
        args.insecure,
        args.output_yaml,
        args.private_token,
        args.project,
        args.ref,
    )


def encode(input):
    input = urllib.parse.quote(input)
    return input.replace("/", "%2F")


def gitlab_ci_linter(
    server, filename, insecure, output_yaml, private_token, project, ref
):
    try:
        gitlab_ci_content = open(filename).read()
    except FileNotFoundError:
        print(f"File not found: {filename}", file=sys.stderr)
        return 1

    if not project:
        project = ""
    project = encode(project)
    params = urllib.parse.urlencode(
        {"include_merged_yaml": "true", "include_jobs": "true"}
    )
    url = f"{server}/api/v4/ci/lint?{params}"
    content = {"content": gitlab_ci_content, "ref": ref}
    if len(project) > 0:
        url = f"{server}/api/v4/projects/{project}/ci/lint?{params}"
        content = {"content": gitlab_ci_content, "content_ref": ref}
    print(f"using {url} ref: {ref} to validate")
    data = json.dumps(content).encode("utf-8")

    r = urllib.request.Request(url, data=data)

    r.add_header("Content-Type", "application/json")

    if private_token:
        r.add_header("PRIVATE-TOKEN", private_token)

    # Verify or not server certificate
    ssl_ctx = ssl.SSLContext() if insecure else None
    with urllib.request.urlopen(r, context=ssl_ctx) as gitlab:
        if gitlab.status not in range(200, 300):
            print(f"Server said {gitlab.status}: {gitlab.url}", file=sys.stderr)
            return 1

        response_raw = gitlab.read()
        response = json.loads(response_raw.decode("utf-8"))

    if response.get("status") == "valid" or response.get("valid"):
        print(f"{filename} is valid")
        return 0
    else:
        print(f"{filename} is invalid (server: {server}):", file=sys.stderr)
        print("\n".join(response["errors"]), file=sys.stderr)
        if output_yaml:
            print(response["merged_yaml"])
        return 1


if __name__ == "__main__":
    sys.exit(main())
